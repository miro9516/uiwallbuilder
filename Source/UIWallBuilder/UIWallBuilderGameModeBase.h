// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "UIWallBuilderGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class UIWALLBUILDER_API AUIWallBuilderGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
