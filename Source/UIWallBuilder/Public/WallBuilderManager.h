// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "WallBuilderManager.generated.h"

class ABuildJoint;
class AWall;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class UIWALLBUILDER_API UWallBuilderManager : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UWallBuilderManager();

	UPROPERTY(BlueprintReadWrite, Category = "Building")
	float WallHeight = 100;

	UFUNCTION(BlueprintCallable, Category = "Building")
	void AddBuildJoint(FVector2D Location);

	UFUNCTION(BlueprintCallable, Category = "Building")
	void CreateWall();

	UFUNCTION(BlueprintCallable, Category = "Building")
	void ClearWorld();

	UPROPERTY(BlueprintReadOnly, Category = "Building")
	TArray<ABuildJoint*> Joints;

	UPROPERTY(BlueprintReadWrite, Category = "Building")
	TSubclassOf<ABuildJoint> JointClass;

	UPROPERTY(BlueprintReadWrite, Category = "Building")
	TSubclassOf<AWall> WallClass;

private:

	void AddLineWall(FVector FirstJointLocation, FVector SecondJointLocation);

	AWall* SpawnedWall = nullptr;

	AWall* SpawnNewWallActor();

};
