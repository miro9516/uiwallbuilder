// Fill out your copyright notice in the Description page of Project Settings.


#include "WallBuilderManager.h"
#include "BuildJoint.h"
#include "Wall.h"
#include "Components/HierarchicalInstancedStaticMeshComponent.h"

// Sets default values for this component's properties
UWallBuilderManager::UWallBuilderManager()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;
}

void UWallBuilderManager::AddBuildJoint(FVector2D Location)
{
	if (!ensure(JointClass)) { return; }

	FVector SpawnLocation = FVector(Location.X, Location.Y, 0);
	FActorSpawnParameters SpawnParams;
	SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	Joints.Add(GetWorld()->SpawnActor<ABuildJoint>(JointClass, SpawnLocation, FRotator::ZeroRotator, SpawnParams));
}

void UWallBuilderManager::CreateWall()
{
	if (!ensure(WallClass)) { return; }
	if (Joints.Num() < 2) { return;	}

	if (SpawnedWall) { SpawnedWall->Destroy(); }
	SpawnedWall = SpawnNewWallActor();

	for (int i = 0; i < (Joints.Num() - 1); i++)
	{
		AddLineWall(Joints[i]->GetActorLocation(), Joints[i + 1]->GetActorLocation());
	}
}

void UWallBuilderManager::AddLineWall(FVector FirstJointLocation, FVector SecondJointLocation)
{
	FVector LineWallVector = (SecondJointLocation - FirstJointLocation).GetSafeNormal();
	FRotator LineRotation = (SecondJointLocation - FirstJointLocation).Rotation();
	float LineWallLength = (SecondJointLocation - FirstJointLocation).Size();
	int NumOfWallInLine = FGenericPlatformMath::CeilToInt(LineWallLength / 100);
	for (int i = 0; i <= NumOfWallInLine - 1; i++)
	{
		FVector InstanceLocation = FirstJointLocation + LineWallVector * 100 * i;
		float WallXScale = 1;
		if (i == NumOfWallInLine - 1)
		{
			WallXScale = (SecondJointLocation - InstanceLocation).Size() / 100;
		}
		FVector WallScale = FVector(WallXScale, 1, WallHeight/100);
		FTransform InstanceTransform = FTransform(LineRotation, InstanceLocation, WallScale);
		SpawnedWall->HierarchicalStaticMeshComp->AddInstanceWorldSpace(InstanceTransform);
	}
}

AWall* UWallBuilderManager::SpawnNewWallActor()
{
	FActorSpawnParameters SpawnParams;
	SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	return GetWorld()->SpawnActor<AWall>(WallClass, FVector::ZeroVector, FRotator::ZeroRotator, SpawnParams);
}

void UWallBuilderManager::ClearWorld()
{
	if (SpawnedWall) { SpawnedWall->Destroy(); }
	for (ABuildJoint* Joint : Joints) {	Joint->Destroy(); }
	Joints.Empty();
}